Cypress.Commands.add('login', function (user) {
    if (user === undefined) throw new Error("Login via API failed because the user credential was not given.");
    cy.request({
        method: 'POST',
        url: "/online/api/auth",
        body: user,
        qs: { "t": new Date().getTime() }
    }).as("response")
});

Cypress.Commands.add('logout', function () {
    if (cy.getCookie("session") != null) {
        cy.request({
            method: 'GET',
            url: "/online/logout",
            qs: { "t": new Date().getTime() }
        }).as("response");
    }
})

Cypress.Commands.add('addFeed', function (aFeed) {
    cy.request({
        method: 'POST',
        url: "/online/api/feed",
        body: {
            "category": aFeed.category,
            "feed": aFeed.feed
        },
        qs: { "t": new Date().getTime() }
    }).then((response) => {
        if (response.status == 200) {
            // collect data :
            // id : number like 3911929
            // category : number or 0 if not defined
        }
    })
})

Cypress.Commands.add('removeFeed', function (aFeedId) {

})